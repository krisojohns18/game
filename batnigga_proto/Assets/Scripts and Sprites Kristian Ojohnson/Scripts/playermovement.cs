﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playermovement : MonoBehaviour
{
    private Rigidbody2D myRigidbody;

    private Animator myAnimator;

    [SerializeField]
    private float movementSpeed;

    private bool attack;

    // to flip batman the right direction
    private bool facingRight;

    private bool jump;

    // Start is called before the first frame update
    void Start()
    {
        facingRight = true;
        myRigidbody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();

    }

    void Update()
    {
        HandleInput();

    }


    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");

        HandleMovement(horizontal);

        Flip(horizontal);

        HandleAttacks();

        ResetValues();
    }

    private void HandleMovement(float horizontal)
    {
        //will check the tag of the animation state and will not let you attack whilst running
        if (!this.myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Punch"))
        {
            myRigidbody.velocity = new Vector2(horizontal * movementSpeed, myRigidbody.velocity.y);

            myAnimator.SetFloat("speed", Mathf.Abs(horizontal));
        }

        {

        }




    }


    private void HandleAttacks()
    {
        if (attack && !this.myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Punch"))
        {
            myAnimator.SetTrigger("punch");
            myRigidbody.velocity = Vector2.zero;
        }
    }

    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jump = true;
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            attack = true;
        }
    }

    private void Flip(float horizontal)
    {
        // if my horizontal is > 0 we are not facing right we need to flip the character but if it is less than 0 and we are facing right we must flip character
        if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight)
        {
            facingRight = !facingRight;

            Vector3 theScale = transform.localScale;

            theScale.x *= -1;

            transform.localScale = theScale;
        }
    }

    private void ResetValues()
    {
        attack = false;



    }
}
                        

                
            

        
        
    

